FROM gcr.io/cloud-solutions-images/janusgraph:v2

RUN cd janusgraph-0.2.0-hadoop2  && ./bin/gremlin-server.sh -i org.apache.tinkerpop gremlin-python 3.2.6
ADD ./custom-init.groovy janusgraph-0.2.0-hadoop2/scripts/
ADD ./hbase-config.properties janusgraph-0.2.0-hadoop2/conf/

